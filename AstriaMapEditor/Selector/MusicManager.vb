﻿Imports System.ComponentModel
Imports System.IO
Imports WMPLib

Public Class MusicManager

    Dim MyParent As MapEditor
    Dim Mp3Player As WindowsMediaPlayer
    Public Sub New(ByRef _Parent As MapEditor)
        InitializeComponent()
        Me.TopMost = True
        MyParent = _Parent
        Mp3Player = New WindowsMediaPlayer()
        Mp3Player.settings.volume = 50

        ' Affichage des musiques
        Dim InfosDirectory1 As New DirectoryInfo(Main.DirectoryApply & "\Musiques")
        If InfosDirectory1.Exists Then
            LoadMusicsDirectory(InfosDirectory1)
        Else
            InfosDirectory1.Create()
            LoadMusicsDirectory(InfosDirectory1)
        End If
    End Sub

    Private Sub LoadMusicsDirectory(InfosDirectory As DirectoryInfo)
        For Each FileName As FileInfo In InfosDirectory.GetFiles
            KryptonListBox1.Items.Add(FileName.Name.Split(".")(0))
        Next

        KryptonListBox1.Refresh()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BT_Valide.Click
        MyParent.MyDatas.Musique = KryptonListBox1.SelectedItem.Split("-")(0)
        MyParent.MyDatas.MusiqueName = KryptonListBox1.SelectedItem
        Main.MenuMap_RefreshControls()
        Mp3Player.controls.stop()
        Me.Dispose()
    End Sub

    Private Sub KryptonListBox1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles KryptonListBox1.SelectedIndexChanged
        Dim infosFile As New FileInfo(Main.DirectoryApply & "\Musiques\" & KryptonListBox1.SelectedItem & ".mp3")
        If infosFile.Exists Then
            Mp3Player.URL = infosFile.FullName
            Mp3Player.controls.play()
        End If
    End Sub

    Private Sub MusicManager_Closing(sender As Object, e As CancelEventArgs) Handles Me.Closing
        Mp3Player.controls.stop()
    End Sub
End Class